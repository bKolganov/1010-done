#include "gamescene.h"

#include "myroundrect.h"

GameScene::GameScene(unsigned scene_x,
                     unsigned scene_y) {
    gameBoard.newGame();
    printBoard();
    setSceneRect(0, 0, scene_x, scene_y);
}


QBrush GameScene::getBrush(int id) {
  switch (id) {
    case NONE_COLOR: return(QBrush(Qt::lightGray));
    case ANGLE_COLOR: return(QBrush(Qt::blue));
    case LINE_COLOR: return(QBrush(Qt::green));
    case SQUARE_COLOR: return(QBrush(Qt::red));
  }
}

int GameScene::newGame() {
    int tmp;
    tmp = gameBoard.getScore();
    gameBoard.newGame();
    printBoard();
    return tmp;
}

void GameScene::setFigures() {
    QVector<QVector<QVector< int > > > figuresVector = gameBoard.getQFigures();
    int prev_width = 0;
    for (int i = 0; i < figuresVector.size(); ++i ){
        DragDropFigure *itm = new DragDropFigure(i,
                                               (rect_size+rect_offset)*prev_width+i*(rect_size-rect_offset),
                                               (rect_size+rect_offset)*gameBoard.size_y()+top_border_offset,
                                               figuresVector[i],
                                               rect_size,
                                               rect_offset);

        if (!figuresVector[i].isEmpty())
            prev_width += figuresVector[i][0].size();
        itm->setFlag(QGraphicsItem::ItemIsMovable);
        addItem(itm);
    }
}

void GameScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
    qreal event_x = event->scenePos().x(),
          event_y = event->scenePos().y();
    DragDropFigure *itm = dynamic_cast<DragDropFigure*> (itemAt(event_x, event_y, QTransform()));
    if(itm) {
        if (gameBoard.putFigureOnBoard(itm->getPos().x() / (rect_size+rect_offset),
                                       itm->getPos().y() / (rect_size+rect_offset),
                                       itm->getId()))
        removeItem(itm);
        printBoard();
    }
    QGraphicsScene::mouseReleaseEvent(event);
}

struct functor_adder {
    GameScene* usr;
    unsigned rect_size, rect_offset, top_border_offset;
    functor_adder(GameScene* _user, unsigned _rect_size, unsigned _rect_offset,
                  unsigned _top_border_offset):
    usr(_user),
    rect_size(_rect_size),
    rect_offset(_rect_offset),
    top_border_offset(_top_border_offset){}

    void operator()(int value, int i, int j){
      MyRoundRect *itm = new MyRoundRect((rect_size+rect_offset)*j,
                                         (rect_size+rect_offset)*i+top_border_offset,
                                         rect_size,
                                         rect_size,
                                         usr->getBrush(value));
      usr->addItem(itm);
    }
};

void GameScene::printBoard() {
    functor_adder *functor = new functor_adder(this,
                                               rect_size,
                                               rect_offset,
                                               top_border_offset);
    clear();
    QString str = "Your score: %1";
    str = str.arg(gameBoard.getScore());
    addText(str);
    gameBoard.iterate_over_board(*functor);
    delete functor;
    setFigures();
}

