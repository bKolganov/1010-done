#ifndef MYROUNDRECT_H
#define MYROUNDRECT_H

#include <QBrush>
#include <QPainter>
#include <QGraphicsItem>


class MyRoundRect : public QGraphicsItem
{
private:
    int x, y;
    int h, w;
    QBrush b;
public:
    MyRoundRect(int X, int Y, int H, int W, QBrush B);
    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget);

    QRectF boundingRect() const;
};

#endif // MYROUNDRECT_H
