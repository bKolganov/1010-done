#include "simplescorestrategy.h"

unsigned SimpleScoreStrategy::score(QVector<int> &cols,
                                    QVector<int> &rows) const {
  unsigned score = 0;
  if (!cols.empty() && !rows.empty()) {
      score = cols.size() * rows.size() * 100;
  } else {
      score = cols.size() + rows.size();
      score *= 10;
  }

  return score;
}
