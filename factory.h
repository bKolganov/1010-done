#ifndef FACTORY_H
#define FACTORY_H
#include "figure.h"
#include <QTime>
class Factory {
private:
    FigureToBoardInterface* generateSmallSquare();
    FigureToBoardInterface* generateBigSquare();
    FigureToBoardInterface* generateDot();
    FigureToBoardInterface* generateVLine(unsigned length);
    FigureToBoardInterface* generateHLine(unsigned length);
    FigureToBoardInterface* generateRightUpAngle();
    FigureToBoardInterface* generateRightDownAngle();
    FigureToBoardInterface* generateLeftUpAngle();
    FigureToBoardInterface* generateLeftDown();
protected:
    static Factory* _self;
    Factory();
public:
    static Factory* instance();
    static bool deleteInstance();
    FigureToBoardInterface* getRandomSquare();
    FigureToBoardInterface* getRandomLine();
    FigureToBoardInterface* getRandomAngle();
    FigureToBoardInterface* getRandomFigure();

};

#endif // FACTORY_H
