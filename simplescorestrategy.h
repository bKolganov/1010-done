#ifndef SIMPLESCORESTRATEGY_H
#define SIMPLESCORESTRATEGY_H

#include "matrixscorestrategy.h"

class SimpleScoreStrategy : public MatrixScoreStrategy
{
  public:
    unsigned score(QVector<int> &cols, QVector<int> &rows) const;
};

#endif // SIMPLESCORESTRATEGY_H
