#include "scoreform.h"
#include "ui_scoreform.h"

void ScoreForm::showEvent(QShowEvent *) {
    QSqlTableModel *model = new QSqlTableModel();
    model->setTable("score");
    model->select();
    model->sort(2, Qt::DescendingOrder);
    ui->tableView->setModel(model);
    ui->tableView->resizeColumnsToContents();
    ui->tableView->hideColumn(0);
}

ScoreForm::ScoreForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ScoreForm)
{
    ui->setupUi(this);

}

ScoreForm::~ScoreForm()
{
    delete ui;
}

void ScoreForm::on_pushButton_clicked()
{
    close();
}
