#ifndef GAMESCENE_H
#define GAMESCENE_H
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsView>
#include <QBrush>

#include "printmyfigure.h"
#include "board.h"

class GameScene : public QGraphicsScene
{
private:
    Board gameBoard;
    static const unsigned rect_size = 35;
    static const unsigned rect_offset = 5;
    static const unsigned top_border_offset = 30;
    static const unsigned scene_default_max_x = 400;
    static const unsigned scene_default_max_y = 720;
protected:
    void setFigures();
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void printBoard();
public:
    GameScene(unsigned scene_x = scene_default_max_x,
              unsigned scene_y = scene_default_max_y);
    int newGame();
    QBrush getBrush(int id);
};

#endif // GAMESCENE_H
