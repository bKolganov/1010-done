#ifndef MATRIXSCORESTRATEGY_H
#define MATRIXSCORESTRATEGY_H

#include <QVector>

class MatrixScoreStrategy
{
  public:
    virtual unsigned score(QVector<int> &cols, QVector<int> &rows) const = 0;
};

#endif // MATRIXSCORESTRATEGY_H
