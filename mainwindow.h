#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include <QtSql>
#include "gamescene.h"
#include "scoreform.h"
#include <QMessageBox>
#include <QDateTime>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0,
                        unsigned window_x = window_x_size,
                        unsigned window_y = window_y_size);
    ~MainWindow();

private slots:
    void on_actionNew_Game_2_triggered();

    void on_actionTop_Results_triggered();

private:
    ScoreForm *sForm;
    GameScene *scn;
    Ui::MainWindow *ui;

    static const unsigned
      window_x_size = 680,
      window_y_size = 820;
};

#endif // MAINWINDOW_H2
