#ifndef MATRIX_H
#define MATRIX_H
#include "figure.h"
#include "matrixscorestrategy.h"
#include <QVector>

class Matrix
{
private:
    int** board;
    int height, width;
    void checkRows(QVector<int> &rowsForDelete);
    void checkCols(QVector<int> &colsForDelete);
    void deleteRows(int n);
    void deleteCols(int n);
    bool checkFreeSpace(int x, int y, Figure *f);
    int deleteFullLines();
    MatrixScoreStrategy *strat;
public:
    int* operator[](unsigned i);
    int setFigure(int x, int y, FigureToBoardInterface *f);
    Matrix(int h, int w);
    ~Matrix();
    unsigned size_x();
    unsigned size_y();
};

#endif // MATRIX_H
