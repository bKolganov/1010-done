#-------------------------------------------------
#
# Project created by QtCreator 2014-12-14T17:09:07
#
#-------------------------------------------------

QT       += core gui
QT += sql
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GUI_for_1010
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    printmyfigure.cpp \
    board.cpp \
    factory.cpp \
    figure.cpp \
    matrix.cpp \
    gamescene.cpp \
    scoreform.cpp \
    myroundrect.cpp \
    simplescorestrategy.cpp

HEADERS  += mainwindow.h \
    printmyfigure.h \
    board.h \
    factory.h \
    figure.h \
    matrix.h \
    gamescene.h \
    scoreform.h \
    myroundrect.h \
    matrixscorestrategy.h \
    simplescorestrategy.h

FORMS    += mainwindow.ui \
    scoreform.ui
