#ifndef BOARD_H
#define BOARD_H
#include "matrix.h"
#include "factory.h"
class Board
{
private:
    Matrix *board;
    Factory* figuresFactory;
    FigureToBoardInterface **addable_figures;
    int score, figuresInArray;
    void setFigures();

    static const unsigned DEFAULT_ADDABLE_NUMBER = 3;
    static const unsigned DEFAULT_BOARD_SIZE = 10;
    unsigned addable_number, board_size;
public:
    void newGame();
    bool putFigureOnBoard(int x, int y, int figureId);

    template<typename funct_type>
    void iterate_over_board(funct_type function);

    FigureToBoardInterface** getFigures();
    QVector<QVector < QVector<int> > > getQFigures();
    unsigned size_y();
    int getScore();
    Board(unsigned _addable_number = DEFAULT_ADDABLE_NUMBER,
          unsigned _board_size = DEFAULT_BOARD_SIZE);
    ~Board();
};

template<typename funct_type>
void Board::iterate_over_board(funct_type function){
  for(unsigned i = 0; i < board->size_x(); i++)
    for(unsigned j = 0; j < board->size_y(); j++)
      function(board->operator[](i)[j], i, j);
}

#endif // BOARD_H
