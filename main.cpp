#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
            QApplication a(argc, argv);
    QSqlDatabase DB;
    DB = QSqlDatabase::addDatabase("QSQLITE");
    DB.setDatabaseName( QString(QCoreApplication::applicationDirPath() +"/score"));
    if (!DB.isOpen()) {
        DB.open();
    }
    QSqlQuery createQuery;
    createQuery.exec("CREATE TABLE IF NOT EXISTS `score` ("
                     "`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"
                     "`date`	TEXT,"
                     "`score`	INTEGER"
                 ");");


    MainWindow w;
    w.show();
    return a.exec();
}
