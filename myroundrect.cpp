#include "myroundrect.h"

MyRoundRect::MyRoundRect(int X, int Y, int H, int W, QBrush B):
  x(X), y(Y), h(H), w(W), b(B){}

void MyRoundRect::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget){
    painter->setBrush(b);
    QPen pen;
    pen.setWidth(0);
    painter->setPen(pen);
    painter->drawRoundedRect(x,y,h,w, 10, 10);
}

QRectF MyRoundRect::boundingRect() const {
    return QRectF(x, y, h, w);
}
