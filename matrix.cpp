#include "matrix.h"

#include "simplescorestrategy.h"

Matrix::Matrix(int h, int w)
{
    board = new int*[h];
    height = h;
    width = w;
    for (int i = 0; i < h; ++i) {
        board[i] = new int[w];
        for (int j = 0; j < w; ++j) board[i][j] = 0;
    }
    strat = new SimpleScoreStrategy();
}

Matrix::~Matrix() {
    for (int i = 0; i < height; ++i) {
        delete board[i];
    }
    delete board;
    delete strat;
}

void Matrix::checkRows(QVector<int> &rowsForDelete) {
  for (int i = 0; i < height; ++i)
       for (int j = 0; j < width && board[i][j] != 0; ++j)
           if( j == width - 1 )
               rowsForDelete.push_back(i);
}

void Matrix::checkCols(QVector<int> &colsForDetele) {
  for (int i = 0; i < width; ++i)
    for (int j = 0; j < height && board[j][i] != 0; ++j)
      if(j == width - 1)
        colsForDetele.push_back(i);
}

void Matrix::deleteRows(int n) {
  for (int i = 0; i < width; ++i)
    board[n][i] = 0;
}

void Matrix::deleteCols(int n) {
  for (int i = 0; i < height; ++i)
    board[i][n] = 0;
}

int Matrix::deleteFullLines() {
  unsigned score = 0;
  QVector<int> colsForDelete;
  QVector<int> rowsForDelete;
  checkCols(colsForDelete);
  checkRows(rowsForDelete);
  score = strat->score(colsForDelete, rowsForDelete);
  for (int i = 0; i < colsForDelete.size(); ++i)
    deleteCols(colsForDelete[i]);
  for (int i = 0; i < rowsForDelete.size(); ++i)
    deleteRows(rowsForDelete[i]);
  return score;
}

bool Matrix::checkFreeSpace(int x, int y, Figure *f) {
    bool result = true;
    int fw = f->width;
    int fh = f->height;
    if ((x + fw > 10) || (y + fh > 10)) return false;
    for (int i = 0; (i < fh); ++i ) {
        for ( int j = 0; (j < fw); ++j) {
            result = !(board[i + y][j + x] && f->figureMatrix[i][j]);
            if (!result) return result;
        }
    }
    return result;
}

int Matrix::setFigure(int x, int y, FigureToBoardInterface *f) {
    if (checkFreeSpace(x, y, f->getFigure())) {
        Figure *fig = f->getFigure();
        int fw = fig->width;
        int fh = fig->height;
        for (int i = 0; i < fh; ++i ) {
            for ( int j = 0; j < fw; ++j) {
                if (fig->figureMatrix[i][j]!=0)
                  board[i + y][j + x] = fig->figureMatrix[i][j];
            }
        }
        return deleteFullLines() + 5;
    } else {
        return -1;
    };
}

unsigned Matrix::size_x(){
  return width;
}

unsigned Matrix::size_y(){
  return height;
}

int* Matrix::operator[](unsigned i){
  return board[i];
}
