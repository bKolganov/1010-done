#include "figure.h"

Figure::Figure(int h, int w, int **fM) {
    figureMatrix = new int*[h];
    height = h;
    width = w;
    for (int i = 0; i < h; ++i) {
        figureMatrix[i] = new int[w];
        for (int j = 0; j < w; ++j) {
           figureMatrix[i][j] = fM[i][j];
        }
    }
}

QVector<QVector<int> > FigureToBoardInterface::getQVector(){
    QVector<QVector<int> > result(height);
    for (int i = 0; i < height; ++i) {
        result[i].resize(width);
        for (int j = 0; j < width; ++j) {
            result[i][j] = figureMatrix[i][j];
        }
    }
    return result;
}

Figure* FigureToBoardInterface::getFigure() {
    return this;
}

Angle::Angle(bool right, bool up) {
    height = 3;
    width = 3;
    figureMatrix = new int* [height];
    for (int i = 0; i< height;++i) {
        figureMatrix[i] = new int [width];
        for (int j = 0; j<width; ++j) {
            figureMatrix[i][j] = 0;
        }
    }
    for (int i = 0; i < height; ++i) {
        if ( right ) figureMatrix[i][0] = ANGLE_COLOR;
        else figureMatrix[i][2] = ANGLE_COLOR;
    }
    for (int i = 0; i < width; ++i) {
        if ( up ) figureMatrix[2][i] = ANGLE_COLOR;
        else figureMatrix[0][i] = ANGLE_COLOR;
    }

}

void Line::initAndFillArray(int h, int w) {
    figureMatrix = new int*[h];
    if (h == 1) {
        figureMatrix[0] = new int[w];
        for (int i = 0; i < w; ++i) {
            figureMatrix[0][i] = LINE_COLOR;
        }
    } else {
        for (int i = 0; i < h; ++i) {
            figureMatrix[i] = new int(LINE_COLOR);
        }
    }
}

Line::Line(int lenght, bool vertical) {
    if ( vertical ) {
        width = 1;
        height = lenght;
    } else {
        width = lenght;
        height = 1;
    }
    initAndFillArray(height, width);
}

void Square::initAndFillArray(int h, int w) {
    figureMatrix = new int*[h];
    for (int i = 0; i < h; ++i) {
        figureMatrix[i] = new int[w];
        for (int j = 0; j < w; ++j) {
            figureMatrix[i][j] = SQUARE_COLOR;
        }
    }
}

Square::Square(bool small) {
    if ( small ) {
        height = width = SMALL_SQUARE_SIZE;
    } else {
        height = width = BIG_SQUARE_SIZE;
    }
    initAndFillArray(height, width);
}

Figure* Figure::operator=(const Figure &f) {
    height = f.height;
    width = f.width;
    if (figureMatrix)
      delete[] figureMatrix;
    figureMatrix = new int*[height];
    for (int i = 0; i < height; ++i) {
        figureMatrix[i] = new int[width];
        for (int j = 0; j < width; ++j) {
           figureMatrix[i][j] = f.figureMatrix[i][j];
        }
    }
}
