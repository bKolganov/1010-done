#include "board.h"
Board::Board(unsigned _addable_number,
             unsigned _board_size):
addable_number(_addable_number),
board_size(_board_size)
{
    board = new Matrix(board_size, board_size);
    figuresFactory = Factory::instance();
    addable_figures = new FigureToBoardInterface*[addable_number];
    score = 0;
}

FigureToBoardInterface** Board::getFigures() {
    FigureToBoardInterface** result;
    result = new FigureToBoardInterface*[addable_number];
    for (int i = 0; i < addable_number; ++i) {
        result[i] = addable_figures[i];
    }
    return result;
}

QVector<QVector < QVector<int> > > Board::getQFigures() {
    QVector<QVector<QVector<int> > > result;
    for (int i = 0; i < addable_number; ++i) {
        if (addable_figures[i])
            result.push_back(addable_figures[i]->getQVector());
        else
            result.push_back(QVector<QVector<int> >());
    }
    return result;
}

void Board::newGame() {
    delete board;
    board = new Matrix(board_size, board_size);
    score = 0;
    setFigures();
}
void Board::setFigures() {
    for (int i = 0; i < addable_number; ++i) {
        addable_figures[i] = figuresFactory->getRandomFigure();
    }
    figuresInArray = addable_number;
}
bool Board::putFigureOnBoard(int x, int y, int figureId) {
    int insertingResult = board->setFigure(x, y, addable_figures[figureId]);
    if ( insertingResult != -1 ) {
        score += insertingResult;
        figuresInArray--;
        delete addable_figures[figureId];
        addable_figures[figureId] = NULL;
        if (figuresInArray == 0) setFigures();
        return true;
    }
    return false;
}

Board::~Board(){
  if (board) delete board;
  if (figuresFactory) delete figuresFactory;
  if(addable_figures) {
    for(unsigned i = 0; i < addable_number; i++) {
      if(addable_figures[i])
        delete addable_figures[i];
    }
    delete[] addable_figures;
  }
}

unsigned Board::size_y(){
  return board_size;
}

int Board::getScore() {
    return score;
}
