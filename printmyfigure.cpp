#include "printmyfigure.h"

DragDropFigure::DragDropFigure(int _id,
                               int _xshift,
                               int _yshift,
                               QVector<QVector < int> > _figure,
                               unsigned _rect_size,
                               unsigned _rect_offset):
id(_id), yshift(_yshift), xshift(_xshift),
figure(_figure), height(figure.size()), width(0),
rect_size(_rect_size), rect_offset(_rect_offset)
{
    if (height)
      width = figure[0].size();
}

QRectF DragDropFigure::boundingRect() const {
    return QRectF(xshift, yshift,
                  width*(rect_size+rect_offset),
                  height*(rect_size+rect_offset));
}

void DragDropFigure::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    for (int i = 0; i < height;++i) {
        for (int j = 0; j < width; ++j) {
            if (figure[i][j]) {
                QPen pen;
                pen.setWidth(0);
                painter->setBrush(getBrush(figure[i][j]));
                painter->drawRoundedRect(xshift + (rect_size+rect_offset)*j,
                                         yshift + (rect_size+rect_offset)*i,
                                         rect_size, rect_size,
                                         2*rect_offset,
                                         2*rect_offset);
            }
        }
    }
}

QPoint DragDropFigure::getPos() {
    return QPoint(this->pos().x() + xshift, this->pos().y() + yshift);
}

int DragDropFigure::getId() {
    return id;
}

QBrush DragDropFigure::getBrush(int id) {
    switch (id) {
    case ANGLE_COLOR: return(QBrush(Qt::blue));
    case LINE_COLOR: return(QBrush(Qt::green));
    case SQUARE_COLOR: return(QBrush(Qt::red));
    }
}

void DragDropFigure::returnBack() {
    this->setPos(0,0);
}

void DragDropFigure::mousePressEvent(QGraphicsSceneMouseEvent *e) {
}


void DragDropFigure::mouseReleaseEvent(QGraphicsSceneMouseEvent *e) {
    returnBack();
    QGraphicsItem::mousePressEvent(e);
}
