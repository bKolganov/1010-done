#include "factory.h"

Factory* Factory::_self = NULL;

Factory::Factory() {
    QTime midnight(0,0,0);
    qsrand(midnight.secsTo(QTime::currentTime()));
}

Factory* Factory::instance() {
    if (!_self) {
        _self = new Factory;
    }
    return _self;
}

bool Factory::deleteInstance() {
    if (_self) {
        delete _self;
        return true;
    }
    return false;
}

// squares begin
FigureToBoardInterface* Factory::generateSmallSquare() {
    FigureToBoardInterface* result = new Square;
    return result;
}
FigureToBoardInterface* Factory::generateBigSquare() {
    FigureToBoardInterface* result = new Square(false);
    return result;
}
// squares end

// vert lines begin
FigureToBoardInterface* Factory::generateDot() {
    FigureToBoardInterface* result = new Line;
    return result;
}

FigureToBoardInterface* Factory::generateVLine(unsigned length){
  return new Line(length);
}
// vert lines end

// hor line begin
FigureToBoardInterface* Factory::generateHLine(unsigned length){
  return new Line(length, false);
}
// hor lines end

// angles begin
FigureToBoardInterface* Factory::generateRightUpAngle() {
    FigureToBoardInterface* result = new Angle;
//    result->printF();
    return result;
}
FigureToBoardInterface* Factory::generateRightDownAngle() {
    FigureToBoardInterface* result = new Angle(true, false);
    return result;
}
FigureToBoardInterface* Factory::generateLeftUpAngle() {
    FigureToBoardInterface* result = new Angle(false);
//    result->printF();
    return result;
}
FigureToBoardInterface* Factory::generateLeftDown() {
    FigureToBoardInterface* result = new Angle(false, false);
    return result;
}
//angles end

// random Figure by types
FigureToBoardInterface* Factory::getRandomAngle() {
    int rndInt = qrand() % 4;
    switch (rndInt) {
    case 0:
        return generateRightUpAngle();
    case 1:
        return generateRightDownAngle();
    case 2:
        return generateLeftUpAngle();
    case 3:
        return generateLeftDown();
    }
}

FigureToBoardInterface* Factory::getRandomSquare() {
    int rndInt = qrand() % 2;
    switch (rndInt) {
    case 0:
        return generateSmallSquare();
    case 1:
        return generateBigSquare();
    }
}

FigureToBoardInterface* Factory::getRandomLine() {
    int rndInt = qrand() % 9;
    generateVLine(rndInt);
}

FigureToBoardInterface* Factory::getRandomFigure() {
    int rndInt = qrand() % 15;
    if (rndInt < 3)
        return getRandomSquare();
    if (rndInt < 6)
        return getRandomAngle();
    return getRandomLine();
}

